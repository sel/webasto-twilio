<?php

    // Step 1: Get the Twilio-PHP library from twilio.com/docs/libraries/php, following the instructions to install it with Composer.
    require_once 'vendor/autoload.php';
    use Twilio\Rest\Client;

    // Display start message on the screen
    echo 'START';

    // Set the timezone.
    date_default_timezone_set('Europe/London');
    $now = date('H').date('i');

    // Load the schedule.
    $schedule = file_get_contents('/Library/WebServer/Documents/data/schedule.json');
    $schedule = json_decode($schedule);

    // Loop over the schedule.
    foreach ($schedule as $day => $times) {

        // Match today.
        if ($day == date('l')) {

            // Loop over the times set for today.
            foreach ($times as $time) {

                // Match now.
                if ($now == $time) {

                    // Step 2: set our AccountSid and AuthToken from https://twilio.com/console
                    $AccountSid = 'AC8e9403f060c243401972171c66a0c24d';
                    $AuthToken = '1cdc149333a968553e179274909999fa';

                    // Step 3: instantiate a new Twilio Rest Client.
                    $client = new Client($AccountSid, $AuthToken);

                    // Step 4: make an array of people we know, to send them a message.
                    $people = array(
                        // '+447944463094' => 'Sel-Vin Kuik',
                        '+447940204790' => 'Toucan II',
                    );

                    // Step 5: Loop over all our friends. $number is a phone number above, and $name is the name next to it.
                    foreach ($people as $number => $name) {

                        $sms = $client->account->messages->create(

                            // The number we are sending to...
                            $number,

                            array(
                                // Step 6: Change the 'From' number below to be a valid Twilio number that you've purchased.
                                'from' => '+441623272293',

                                // The SMS body...
                                'body' => 'START'
                            )

                        );

                        // Display a confirmation message on the screen.
                        echo ' | START message executed';

                    }

                }

            }

        }

    }

    // Display end message on the screen.
    echo ' | END';
