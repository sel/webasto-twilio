$(function() {

    /**
     * Load the data in to the page
     */

    $.ajax({
        url: '../data/api.php',
        success: function(SCHEDULE) {

            for (let day in SCHEDULE) {
                for (let time of SCHEDULE[day]) {
                    $('[data-day=' + day + ']').addTimer(time.substring(0, 2), time.substring(2, 4));
                }
            }
        
        }
    });


    /**
     * Handle clicks on pluses
     */

    $(document.body).on('click', '.plus', function() {
        $(this).parents('[data-day]').addTimer();

        saveData();
    });


    /**
     * Handle clicks on minuses
     */

    $(document.body).on('click', '.minus', function() {
        $(this).parent().remove();

        saveData();
    });

    
    /**
     * Handle clicks on arrows
     */

    $(document.body).on('click', '.arrow', function() {
        var $this = $(this),
                direction = $this.hasClass('arrow--up') ? '+' : '-',
                unit = $this.parent().hasClass('time--hour') ? 'h' : 'm',
                $readout = $this.siblings('.time__readout');

        $readout.updateReadout(direction, unit);

        saveData();
    });
    
});


/**
 * Component for saving data
 */

function saveData() {
    var SCHEDULE = {};

    $('[data-day]').each(function(dayIndex, dayElement) {
        var $dayElement = $(dayElement);

        SCHEDULE[$dayElement.attr('data-day')] = [];

        $dayElement.find('.time').each(function(timeIndex, timeElement) {
            var $readout= $(timeElement).find('.time__readout');

            SCHEDULE[$dayElement.attr('data-day')].push($readout[0].innerHTML + $readout[1].innerHTML);
        });
    });

    $.ajax({
        url: '../data/api.php',
        method: 'post',
        data: SCHEDULE
    });
}

/**
 * Component for adding timers to the page
 */

(function($, addTimer) {
    
    $.fn[addTimer] = function(hour, minute) {
        if (!hour) {
            hour = '00';
        }
        
        if (!minute) {
            minute = '00';
        }
        
        this.find('.plus').before('<div class="time">\
            <svg class="minus" viewBox="0 0 129 129">\
                <path d="m64.5,122.4c31.9,0 57.9-26 57.9-57.9s-26-57.9-57.9-57.9-57.9,26-57.9,57.9 26,57.9 57.9,57.9zm0-107.7c27.4-3.55271e-15 49.8,22.3 49.8,49.8s-22.3,49.8-49.8,49.8-49.8-22.4-49.8-49.8 22.4-49.8 49.8-49.8z" fill="#D80027"/>\
                <path d="M37.8,68h53.3c2.3,0,4.1-1.8,4.1-4.1s-1.8-4.1-4.1-4.1H37.8c-2.3,0-4.1,1.8-4.1,4.1S35.6,68,37.8,68z" fill="#D80027"/>\
            </svg>\
            <div class="time__unit time--hour">\
                <svg class="arrow arrow--up" viewBox="0 0 7 14">\
                    <polyline fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,1 6,7 1,13"></polyline>\
                </svg>\
                \
                <div class="time__readout">' +
                    hour
                + '</div>\
                \
                <svg class="arrow arrow--down" viewBox="0 0 7 14">\
                    <polyline fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,1 6,7 1,13"></polyline>\
                </svg>\
            </div>\
            \
            <div class="time__unit time--minute">\
                <svg class="arrow arrow--up" viewBox="0 0 7 14">\
                    <polyline fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,1 6,7 1,13"></polyline>\
                </svg>\
                \
                <div class="time__readout">' +
                    minute
                + '</div>\
                \
                <svg class="arrow arrow--down" viewBox="0 0 7 14">\
                    <polyline fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,1 6,7 1,13"></polyline>\
                </svg>\
            </div>\
        </div>');
    }
})($, 'addTimer');


/**
 * Component for dealing with the logic to increment/decrement time
 */

(function($, updateReadout) {

    $.fn[updateReadout] = function(direction, unit) {
        var currentValue = parseInt(this.text()),
                newValue = 0;

        if (direction == '-' && unit == 'h' && !currentValue) {
            newValue = 23;
        }
        else if (direction == '-' && unit == 'h' && currentValue > 1) {
            newValue = currentValue - 1;
        }
        else if (direction == '+' && unit == 'h' && currentValue < 23) {
            newValue = currentValue + 1;
        }
        
        else if (direction == '-' && unit == 'm' && !currentValue) {
            newValue = 45;
        }
        else if (direction == '-' && unit == 'm' && currentValue > 15) {
            newValue = currentValue - 15;
        }
        else if (direction == '+' && unit == 'm' && currentValue < 45) {
            newValue = currentValue + 15;
        }
        
        return this.text(newValue < 10 ? '0' + newValue : newValue);
    };
    
})($, 'updateReadout');
