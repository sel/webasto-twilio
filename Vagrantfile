# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "box-cutter/ubuntu1604"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "./", "/vagrant",
    type: "nfs"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "512"

    # Customise the VirtualBox machine name
    vb.name = "Webasto"
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "root-user", type: "shell", inline: <<-SHELL
    # Update package repositories
    sudo apt-get update

    # Upgrade existing packages
    sudo apt-get dist-upgrade -y

    # Install new packages
    sudo apt-get install -y apache2 libapache2-mod-php7.0 php7.0 npm

    # Install Composer
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

    # Configure Apache
    echo "ServerName localhost" | sudo tee --append /etc/apache2/apache2.conf
    sudo a2enmod rewrite headers
    sudo ln -s /vagrant/webasto-twilio.conf /etc/apache2/sites-enabled/webasto-twilio.conf

    # If there Apache configs are linked to the shared folder it will fail to start,
    # start Apache only once shared folders have been mounted
    sudo service apache2 start
  SHELL

  config.vm.provision "vagrant-user", type: "shell", privileged: false, inline: <<-SHELL
    # Fix npm permissions
    sudo mkdir $(npm config get prefix)/{lib/node_modules,n}
    sudo chown -R vagrant $(npm config get prefix)/{lib/node_modules,n,bin,include,share}

    # Update node
    npm cache clean -f
    npm install -g n
    n latest

    # Install Gulp/Bower
    npm install -g gulp-cli
  SHELL
end
