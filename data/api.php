<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	header('Content-Type: application/json');

	echo file_get_contents('schedule.json');
}

else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	// var_dump($_POST);
	file_put_contents('schedule.json', json_encode($_POST));
}